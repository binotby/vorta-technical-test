# Vorta Technical Test

## Question 1
Your company is running a test that is designed to compare three different versions of the company’s website. Version X of the website is shown to 45% of users, version Y of the website is shown to 30%, while version Z the remaining 25% of users. The test shows that 12% of users who are presented with version X sign up for the company’s services, compared to 8% of users who are presented with version Y and 10% sign up with version Z. If a user signs up for the company’s services, what is the probability that she/he was presented with version X of the website?
### Answer:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Y&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Z<br />
User
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;45
&nbsp;&nbsp;&nbsp;30
&nbsp;&nbsp;25
<br />
Sign Up
&nbsp;&nbsp;&nbsp;&nbsp;12
&nbsp;&nbsp;&nbsp;&nbsp;8
&nbsp;&nbsp;&nbsp;&nbsp;10
<br />

PX = (45 x 12) / (45 x 12) + (30 x 8) + (25 x 10)
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = 540 / (540 + 240 + 250)<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = 540 / 1030<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = 0.524 x 100%<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = 52%<br />

the probability presented with version X of the website is **52%**
## Question 2
Write a program that prints the numbers from 1 to 100. But for multiples of 3 print “Fizz” instead of the number and for the multiples of 5 print “Buzz”. For numbers which are multiples of both 3 and 5 print “FizzBuzz”. Write with the most convenient programming language for you with proper explanation in your code.
### Answer:
```python
modulo_list = [(3,"Fizz"), (5,"Buzz")]
 
for i in range(1, 101):
    print_string = ""
    for mod in modulo_list:
        if i % mod[0] == 0:
            print_string += mod[1]

    if print_string == "":
        print(i)
    else:
        print(print_string)

```
**Explanation:**
- Define the modulo list which will check for the 3 and 5 divisors using ```modulo_list = [(3,"Fizz"), (5,"Buzz")]```
- Iterate through the list output using ```for i in range(1, 101): ```
- ```print_string = ""``` Initialize an empty string which we will print after checking the divisors.
- For each number up to 100 check if that number is visible by the numbers defined in the modulo list by iterating over that list and checking ```if i % mod[0] == 0:```.
- If the number is visible by one of those values than join it's print word defined by mod[1] ```print_string += mod[1]```onto the output string
- If the print_string is empty then print the number <tt>print(i)</tt>
- If the print_string was set by something because the current number was visible than print the print_string ```print(print_string)```

## Question 3
Vorta is currently developing a secure document management system, that provides a dashboard to monitor every uploaded documents from our mobile app. The mobile app and the dashboard can be seen as below:

<img src="https://gitlab.com/binotby/vorta-technical-test/-/raw/main/Assets/Secure_Data_Management__Desktop_.png" width="49%"/>
<img src="https://gitlab.com/binotby/vorta-technical-test/-/raw/main/Assets/Secure_Data_Management__Mobilee_.png" width="49%"/>

From the case above, please do the following tasks:
1. Create the software architecture of the aplication, from the database, storage, backend, and frontend.
2. Create the business process (flow chart/etc.) of the system and database
design for this case (user can upload any documents from mobile app, and admin can check the document and its activity logs).
3. Create the endpoints to:
    1. Register
    2. Login
    3. Upload Documents -> You can use cloud storage such as cloudinary or S3.

The program should be run in users's device.
### Answer:
1. Software Architecture 
<img src="https://gitlab.com/binotby/vorta-technical-test/-/raw/main/Assets/Software_Architecture.jpeg" width="100%"/>
2.1 Flow chart
<img src="https://gitlab.com/binotby/vorta-technical-test/-/raw/main/Assets/Flow_Chart.jpeg" width="100%"/>
2.2 Database design
<img src="https://gitlab.com/binotby/vorta-technical-test/-/raw/main/Assets/Database_Design.jpeg" width="100%"/>
3. Backend code can be view on <a href="https://gitlab.com/binotby/vorta-technical-test/-/tree/main/api">Api Folder</a>
