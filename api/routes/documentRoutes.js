const express = require("express");
const fileUpload = require("express-fileupload");
const router = express.Router();
const documentsController = require("../controllers/documentsController");
const fileExtLimiter = require("../middleware/fileExtLimiter");
const fileSizeLimiter = require("../middleware/fileSizeLimiter");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
    .route("/")
    .get(documentsController.getAllDocuments)
    .post(
        fileUpload({ useTempFiles: true }),
        fileExtLimiter([".png", ".jpg", ".jpeg"]),
        fileSizeLimiter,
        documentsController.createNewDocument
    )
    .patch(documentsController.updateDocument)
    .delete(documentsController.deleteDocument);

module.exports = router;
