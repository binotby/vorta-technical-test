const mongoose = require('mongoose')

const documentSchema = new mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        files: {
            type: Array,
            assetId: [{ type: String, required:true }],
            title: [{ type: String, required:true }],
            format: [{ type: String, required:true }],
            size: [{ type: Number , required:true}],
            url: [{ type: String , required:true}],
        },
    },
    {
        timestamps: true
    }
)

module.exports = mongoose.model('Document', documentSchema)