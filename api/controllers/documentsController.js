const Document = require("../models/Document");
const User = require("../models/User");
const cloudinary = require("../utils/cloudinary");

// @desc Get all documents
// @route GET /documents
// @access Private
const getAllDocuments = async (req, res) => {
    // Get all documents from MongoDB
    const documents = await Document.find().lean();

    // If no documents
    if (!documents?.length) {
        return res.status(400).json({ message: "No documents found" });
    }

    // Add username to each document before sending the response
    const documentsWithUser = await Promise.all(
        documents.map(async (document) => {
            const user = await User.findById(document.user).lean().exec();
            return { ...document, username: user.username };
        })
    );

    res.json(documentsWithUser);
};

// @desc Create new document
// @route POST /documents
// @access Private
const createNewDocument = async (req, res) => {
    const { title, description } = req.body;
    let { documents } = req.files;
    const user = req.user;

    if (!(documents instanceof Array)) documents = [documents];

    // Confirm data
    if (!title || !description || !documents.length) {
        return res.status(400).json({ message: "All fields are required" });
    }

    // Check for duplicate title
    const duplicate = await Document.findOne({ title }).collation({ locale: "en", strength: 2 }).lean().exec();

    if (duplicate) {
        return res.status(409).json({ message: "Duplicate document title" });
    }

    let uploadedFiles = [];
    for (const document of documents) {
        const cloudinaryResponse = await cloudinary.upload(document.tempFilePath, "Documents");
        uploadedFiles.push({
            asset_id: cloudinaryResponse.asset_id,
            title: cloudinaryResponse.original_filename,
            format: cloudinaryResponse.format,
            size: cloudinaryResponse.bytes,
            url: cloudinaryResponse.secure_url,
        });
    }

    // Create and store the new user
    const document = await Document.create({
        user: user.id,
        title: title,
        description: description,
        files: uploadedFiles,
    });

    if (document) { // Created
        return res.status(201).json({ message: "New document created" });
    } else {
        return res.status(400).json({ message: "Invalid document data received" });
    }
};

// @desc Update a document
// @route PATCH /documents
// @access Private
const updateDocument = async (req, res) => {
    const { id, user, title, text, completed } = req.body;

    // Confirm data
    if (!id || !user || !title || !text || typeof completed !== "boolean") {
        return res.status(400).json({ message: "All fields are required" });
    }

    // Confirm document exists to update
    const document = await Document.findById(id).exec();

    if (!document) {
        return res.status(400).json({ message: "Document not found" });
    }

    // Check for duplicate title
    const duplicate = await Document.findOne({ title }).collation({ locale: "en", strength: 2 }).lean().exec();

    // Allow renaming of the original document
    if (duplicate && duplicate?._id.toString() !== id) {
        return res.status(409).json({ message: "Duplicate document title" });
    }

    document.user = user;
    document.title = title;
    document.text = text;
    document.completed = completed;

    const updatedDocument = await document.save();

    res.json(`'${updatedDocument.title}' updated`);
};

// @desc Delete a document
// @route DELETE /documents
// @access Private
const deleteDocument = async (req, res) => {
    const { id } = req.body;

    // Confirm data
    if (!id) {
        return res.status(400).json({ message: "Document ID required" });
    }

    // Confirm document exists to delete
    const document = await Document.findById(id).exec();

    if (!document) {
        return res.status(400).json({ message: "Document not found" });
    }

    const result = await document.deleteOne();

    const reply = `Document '${result.title}' with ID ${result._id} deleted`;

    res.json(reply);
};

module.exports = {
    getAllDocuments,
    createNewDocument,
    updateDocument,
    deleteDocument,
};
