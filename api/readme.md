# API Docs
## Authorization
To authenticate an API request, you should provide your API key in the `Authorization` header.

**Login**

`POST` `/auth/login`

| Parameter | Type | Description |
| ------ | ------ | ------ |
| `username` | `string` | **Required**. |
| `password` | `string` | **Required**. |

*Response*

```javascript
{
    "accessToken": "ezNhNDwiaWF0IjoxNjY0MTM3ODg4LCJleHAiOjE2NjQxMzg3ODh9.dAmfL5Rt1_XvuvFdZ_KIGuqqCZqkRLOlkJj5fQyZrT4"
}
```

**Register**

`POST` `/auth/register`

| Parameter | Type | Description |
| ------ | ------ | ------ |
| `username` | `string` | **Required**. |
| `password` | `string` | **Required**. |

*Response*

```javascript
{
    "accessToken": "ezNhNDwiaWF0IjoxNjY0MTM3ODg4LCJleHAiOjE2NjQxMzg3ODh9.dAmfL5Rt1_XvuvFdZ_KIGuqqCZqkRLOlkJj5fQyZrT4"
}
```

## Documents

**Register**

`POST` `/documents/`

| Parameter | Type | Description |
| ------ | ------ | ------ |
| `title` | `string` | **Required**. |
| `description` | `string` | **Required**. |
| `documents` | `file` | **Required**. |

*Response*

```javascript
{
    "message": "New document created"
}
```
